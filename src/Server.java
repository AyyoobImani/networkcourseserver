import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.util.HashMap;


public class Server {

	/**
	 * @param args
	 */
	
	public static void main(String[] args) {
		byte[] receiveData = new byte[1024];             
		try {
			socket = new DatagramSocket(4445);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		while(true){
			 System.out.println("listening...");
			DatagramPacket packet = new DatagramPacket(receiveData, 1024);
			try {
				socket.receive(packet);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String data = new String( packet.getData());
			System.out.println("received: "+data);
			
			InetSocketAddress socketAddress = (InetSocketAddress) packet.getSocketAddress();
			
			
			double randNum = Math.random();
			if(randNum<0.25){
				System.out.println("packet Ignored");
				sendNack(getReqSeqNom(socketAddress), socketAddress);
			}
			else{
				System.out.println("packet accepted");
				int seqNom = getPcktSeqNom(data);
				if(validateSeqNom(seqNom, socketAddress)){
					System.out.println("seq nom is valid");
					saveData(data);
					incSeqNom(socketAddress);
					sendNack(getReqSeqNom(socketAddress), socketAddress);
					
				}
				else{
					System.out.println("seq nom is not valid");
					sendNack(getReqSeqNom(socketAddress), socketAddress);
				}
					
				}
			
			
		}

	}
	
	private static HashMap<InetSocketAddress, Integer> clients = new HashMap<InetSocketAddress, Integer>();
	private static DatagramSocket socket = null;
	private BufferedReader in = null;
	
	private static FileWriter fWriter = null;



	/**
	 * 
	 * @param seqNum
	 * @param destination
	 * @return 
	 */
	private static void sendNack(int seqNom, InetSocketAddress address) {
		byte[] sendData = new byte[1024];
		char bSeqNom = (char) seqNom;
		String packet = "NAK" + bSeqNom;
		sendData = packet.getBytes();
		DatagramPacket sendPacket = null;
		try {
			sendPacket = new DatagramPacket(sendData,sendData.length,address);
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			socket.send(sendPacket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("nack sent: " + packet.substring(0,3) + (int) packet.charAt(3));
	}

	/**
	 * 
	 * @param sender
	 * @param String
	 * @return 
	 */
	private static void saveData(String data ) {
		System.out.println("saving data: " + data.substring(0,7) + ( (int) data.charAt(7)) + ( (int) data.charAt(8) ) + data.substring(9));
		
		String clientName = data.substring(0, 7);
		int dataLengh = (int) data.charAt(8);
		String message = data.substring(9,9+dataLengh);
		
		try {
			
			fWriter = new FileWriter("log.txt",true);
			
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		try {
			fWriter.write(clientName + " "+ String.valueOf(getPcktSeqNom(data)) + ": " + message + "\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			//fWriter.close();
			fWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("data saved");
	}


	private static boolean validateSeqNom(int seqNom, InetSocketAddress socketAddress){
		if(clients.containsKey(socketAddress)){
			System.out.println("expectd seq nom: " + clients.get(socketAddress) + "packet seq nom: " + seqNom);
			if(seqNom == (clients.get(socketAddress)  ) ){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			clients.put(socketAddress, 0);
			return true;
		}
		
	}

	/**
	 * 
	 * @param sender
	 * @return 
	 */
	
	private static int getPcktSeqNom(String packet){
		return (int) (packet.charAt(7));
	}
	
	

	private static void incSeqNom(InetSocketAddress address){
		clients.put( address, ( (clients.get(address)+1) % 255) );
	}
	
	private static int getReqSeqNom(InetSocketAddress socketAddress){
		if(clients.containsKey(socketAddress)){
			return clients.get(socketAddress) ;
		}
		else{
			return 0;
		}
		
	}

}
